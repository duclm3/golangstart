module hello

go 1.16

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	gorm.io/driver/mysql v1.0.4 // indirect
	gorm.io/gorm v1.21.3 // indirect
	xorm.io/xorm v1.0.7 // indirect
)
