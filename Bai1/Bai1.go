package Bai1

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
)

type Serve struct {
	Name  string `json: "name"`
	Class string `json: "class"`
}

type User struct {
	Name      string
	Age       int64
	Gender    bool
	Address   string

}

func (s Serve) toString() string {
	bytes, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return string(bytes)
}

func GetServe() []Serve {
	serves := make([]Serve, 3)
	raw, err := ioutil.ReadFile("./Bai1/serve.json")

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	json.Unmarshal(raw, &serves)
	return serves
}

func SortArray() {
	s := []int{11, 34, 56, 77, 99, 109, 66, 20, 88, 34}
	sort.Ints(s)
	fmt.Println("Giá trị sau khi sắp xếp")
	fmt.Println(s)

	// c := make([]string, len(s))
	l1 := s[1:7]
	fmt.Println("Giá trị từ 1 đến 7", l1)
	// l2, err := s[1:15]
	// if err != nil {
	// 	fmt.Println("Giá trị từ 1 đến 15", l2)
	// }

}

// Phần 3 cấu chúc getter setter
func (u User) GetName() string {
	return u.Name
}
func MUser() {
	m := make(map[string]string)

	for i := 0; i < 10; i++ {
		var str1 string
		str1 = "Duc" + strconv.Itoa(i)
		a := &User{str1, 30, true, "Đại la"}
		out, err := json.Marshal(a)
		if err != nil {
			panic(err)
		}
		m[str1] = string(out)
	}
	var items = [][]string{}
	for key, value := range m {
		items = append(items, []string{key, value})
	}
	fmt.Println("Chuyển từ map thành slice thành công", items)
}
