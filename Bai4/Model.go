package Bai4

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
)

type User struct {
	Id         string
	Name       string
	Birth      int64
	Created    int64
	Updated_at int64
}
func (u *User) setUser(id string, name string, birth int64, created int64, updated_at int64) {
   u.Id = id
   u.Name = name
   u.Birth =birth
   u.Created =created
   u.Updated_at = updated_at
}

// Name receives a copy of Foo since it doesn't need to modify it.
func (u User) getUser() User {
    return u;
}
var x *xorm.Engine

type Point struct {
	User_id    string
	Point      int64
	Max_points int64
}

func Migration() {
	x, err := xorm.NewEngine("mysql", "root:1234@/test")

	if err = x.Sync2(new(User)); err != nil {
		log.Fatalf("Fail to sync database user: %v\n", err)
	}
	if err = x.Sync2(new(Point)); err != nil {
		log.Fatalf("Fail to sync database pointer: %v\n", err)
	}
	if err != nil {
		log.Println(err)
	}

	var user *User = new(User)
	user.setUser("12412421","14124214",214124124124214,21412414124,124124124124)
	
	user.InsertUser()
	defer x.Close()
}
func (u *User) InsertUser() {
	affected, err := x.Insert(&u)
	if err != nil {
		log.Println(err)
	}
	log.Println(affected)
}
