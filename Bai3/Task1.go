package Bai3

import (
	"log"
	"sync"
	"time"
)

func chanRoutineMutex(k int) {
	switch k {
	case 1:
		log.Print("hello 1")
		var m = &sync.Mutex{}
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {

			m.Lock()
			log.Print("hello 3")
			m.Unlock()
			wg.Done()
		}()

		wg.Wait()
		log.Print("hello 2")

	default:
		log.Print("hello 1")
		var m = &sync.Mutex{}
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			m.Lock()
			log.Print("hello 3")
			m.Unlock()
			wg.Done()
		}()

		log.Print("hello 2")
		wg.Wait()
	}

}
func chanRoutineChannel(k int) {
	switch k {
	case 1:
		log.Print("hello 1")
		ch := make(chan bool)
		go func() {

			log.Print("hello 3")
			ch <- true
		}()
		<-ch
		log.Print("hello 2")
	default:
		log.Print("hello 1")
		ch := make(chan bool)
		go func() {
			log.Print("hello 3")
			ch <- true
		}()

		log.Print("hello 2")
		<-ch
	}

}
func chanRoutineWaitGroup(k int) {
	switch k {
	case 1:
		var wg sync.WaitGroup
		log.Print("hello 1")

		wg.Add(1)
		go func() {
			defer wg.Done()
			log.Print("hello 2")
			log.Print("hello 3")

		}()

		wg.Wait()

	default:
		var wg sync.WaitGroup
		log.Print("hello 1")
		wg.Add(1)
		go func() {

			log.Print("hello 3")
			wg.Done()
		}()
		wg.Wait()

		log.Print("hello 2")
	}
}
func Task1() {
	// log.Println("Sử dụng mutex:")
	// log.Println("theo thứ tự: ")

	// chanRoutineMutex(3)
	// time.Sleep(2 * time.Second)
	// log.Println("không theo thứ tự: ")
	// chanRoutineMutex(1)
	log.Println("Sử dụng Channel:")

	log.Println("Theo thứ tự: ")
	chanRoutineChannel(3)
	time.Sleep(2 * time.Second)
	log.Println("Không theo thứ tự: ")
	chanRoutineChannel(1)

	// log.Println("Sử dụng WaitGroup: ")
	// log.Println("Theo thứ tự: ")
	// chanRoutineWaitGroup(1)
	// time.Sleep(2 * time.Second)
	// log.Println("Không theo thứ tự: ")
	// chanRoutineWaitGroup(3)
}
