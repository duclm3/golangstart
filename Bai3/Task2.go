package Bai3

import (
	"log"
	"strconv"
	"sync"
)

var m = &sync.Mutex{}
var arr = make(map[string]string)

func Task2() {

	var wg sync.WaitGroup
	for w := 0; w < 3; w++ {
		wg.Add(1)

		go routine(&wg, w)
	}
	wg.Wait()
	log.Println("Độ dài: ", len(arr))
	for k := range arr {
		log.Println("giá trị+"+k+": ", arr[k])
	}
}
func routine(wg *sync.WaitGroup, work int) {
	defer wg.Done()

	for i := 0 + (1000 * work); i < 1000+1000*work; i++ {
		log.Println(i)
		if len(arr) >= 15 {
			return
		}
		m.Lock()
		arr[strconv.Itoa(i)] = strconv.Itoa(i)
		m.Unlock()

	}

}
