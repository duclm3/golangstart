package Bai3

import (
	"log"
	"sync"
)

func Task3() {

	// log.Println("Lỗi là: concurrent map read and map write")
	errFunc()
}
func errFunc() {
	m := make(map[int]int)
	var mu sync.Mutex
	for i := 0; i < 1000; i++ {
		log.Println(i)
		go func() {

			for j := 1; j < 10000; j++ {
				mu.Lock()
				if _, ok := m[j]; ok {

					delete(m, j)
					// log.Println("ok")
					continue
				}
				
				m[j] = j * 10
				mu.Unlock()
			}
		}()

	}

	log.Print("done")
}
