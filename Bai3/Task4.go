package Bai3

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
)

/***
4. bài tập worker pool: tạo bằng tay file dưới. `file.txt` sau đó đọc từng dòng file này nạp dữ liệu vào 1 buffer channel có size 10, Điều kiện đọc file từng dòng. Chỉ được sử dụng 3 go routine. Kết quả xử lý xong ỉn ra màn hình + từ `xong`
```txt
"z9nnHLy8V8"
"6AVcSrDUkB"
"DezRGPwtx7"
"eSmXGjCmTq"
"9rfCMntQA5"
"Trk6xppMuM"
"2sb8BPaUsp"
"6AAh6zVFNA"
"gsY8kAuKp8"
"FQgb8QEpxg"
"hEXnKUkYrp"
"tchiG2Tiv4"
"daMPMJWaM6"
"WbBMpX89Sz"
"YVnsveajtj"
"L9TA7FE5d9"
"xBjE7UNe98"
"q6bLPeVjYr"
"oBppTK62nT"
"GxUjEDYBdG"
"ZTEpXFStLo"
"4XkynbWFvU"
"WFmmUSWzDv"
"nit8qjmvZH"
"iT8BqzHdXo"
"7N7mz3qzn2"
"KfhMZsHABi"
"M4mKWrGgDn"
"qLEduDF7so"
"YhigrGfLJr"
"f82gk2mrxv"
"q7TPNZB3Bv"
"eWLL5Yg6sG"
"GyPqxrXiUg"
"86dGJYRzPN"
"EWYtAVfXnd"
"8dNcD3F8uS"
"NLRE6LKqCt"
"UbLD2DACiB"
"JeLHTTg8vw"
```
nâng cao. In ra số lượng goroutine đã khởi tạo.
hoàn thiện để tối ưu, thu hồi channel và goroutine đã cấp phát.

- Nâng cao 1: Tạo 1 struct `Line` có trường gồm có: `số dòng hiện tại`, `giá trị` của dòng đó.
In ra màn hình cú pháp `${line_number} giá trị là: ${data}`.
- Nâng cao 2: Khi kết thúc chương trình đã cho đóng những vòng lặp vô hạn của các goroutine lại. Viết chương trình đó.
Giợi ý sử dụng biến `make([]chan bool, n)`

```



***/
type Line struct {
	id    int
	value string
}

func (l *Line) setLine(id int, value string) {
	l.id = id
	l.value = value
}
func (l *Line) getLine() string {
	return strconv.Itoa(l.id) + " giá trị là: " + l.value
}
func readCode(path string) int {
	file, err := os.Open(path)
	var finish = make(chan bool, 10)
	if err != nil {
		log.Fatal(err)
	}

	jobs := make(chan string)
	results := make(chan string)

	wg := new(sync.WaitGroup)

	for w := 1; w <= 3; w++ {
		wg.Add(1)
		go worker(jobs, results, wg)
	}

	go func() {
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {

			jobs <- scanner.Text()
		}
		close(jobs)
	}()

	go func() {
		wg.Wait()
		close(results)

	}()

	counts := 0
	for v := range results {
		counts++
		item := Line{id: counts, value: v}
		fmt.Println(item.getLine())
	}
	fmt.Println("Xong")
	close(finish)
	return counts
}

func worker(jobs <-chan string, results chan<- string, wg *sync.WaitGroup) {

	defer wg.Done()
	for v := range jobs {
		results <- v
	}

}

func Task4() {
	// An artificial input source.  Normally this is a file passed on the command line.
	const input = "./Bai3/file.txt"
	numberCode := readCode(input)
	fmt.Println(numberCode)
}
