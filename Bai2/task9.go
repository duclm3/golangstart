package Bai2

import (
	"context"
	"sync"
	"time"
)

type Scheduler struct {
	wg           *sync.WaitGroup
	cancelations []context.CancelFunc
}
type Job func(ctx context.Context)

func (s *Scheduler) process(ctx context.Context, j Job, interval time.Duration) {
	ticker := time.NewTicker(interval)
	for {
		select {
		case <-ticker.C:
			j(ctx)
		case <-ctx.Done():
			s.wg.Done()
			return
		}
	}

}
func NewScheduler() *Scheduler {
	return &Scheduler{
		wg:           new(sync.WaitGroup),
		cancelations: make([]context.CancelFunc, 0),
	}
}
func (s *Scheduler) Add(ctx context.Context, j Job, interval time.Duration) {
	ctx, cancel := context.WithCancel(ctx)
	s.cancelations = append(s.cancelations, cancel)
	s.wg.Add(1)
	go s.process(ctx, j, interval)
}
func (s *Scheduler)Stop(){
	for _,cancel :=range s.cancelations{
		cancel()
	}
	s.wg.Wait()
}