package Bai2

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"time"
)

func CountTime() {

	for i := 0; i < 10; i++ {
		now := time.Now().UnixNano() / 1000
		fmt.Println("time now:", now)
		if i < 2 {
			time.Sleep(3 * time.Second)
		}

		if i == 2 {
			fmt.Println("Kết thúc")
			break
		}
	}

}
func msToTime(ms int64) string {

	t := time.Unix(0, ms*int64(time.Millisecond)).String()
	return t
}
func Task2(unixTime int64) int64 {

	// t := time.Unix(unixTime, 0)
	// strDate := t.Format(time.UnixDate)
	songayhientai := unixTime / (60 * 60 * 24)
	return songayhientai
}
func CalculatorMinute() {
	init := int64(1592190294764144364 / 1000)

	split := strings.Split(msToTime(init), " ")
	fmt.Println(msToTime(init))
	split1 := strings.Split(split[1], ":")
	fmt.Println("Số phút là: ", split1[1])
	fmt.Println("Số phút là: ", 1592190294764144364/(1000*1000*60*24*365))
}
func Task3() {

	// for i := 0; i < 3; i++ {
	// 	time.Sleep(3 * time.Second)
	// 	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	// 	defer cancel()
	// 	var check bool
	// 	check = doSomeThing(ctx)
	// 	if(check){
	// 		break
	// 	}
	// }
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	for i := 0; i < 3; i++ {
		select {
		case <-ctx.Done():
			fmt.Println(ctx.Err())
			cancel()

		default:
			time.Sleep(3 * time.Second)
			fmt.Println("Tiến trình: ", i)

		}
	}
}
func doSomeThing(ctx context.Context) bool {
	cancelChannel := make(chan bool)
	go func() {
		select {
		case <-ctx.Done():
			fmt.Println(ctx.Err())
			cancelChannel <- true
			return
		}
	}()
	isChannelClose := <-cancelChannel
	if isChannelClose {
		close(cancelChannel)
		return true
	}
	fmt.Println("end")
	return false
}
func Task5() {
	var unixTime int64 = 1592190385
	t := time.Unix(unixTime, 0)
	strDate := t.Format(time.UnixDate)
	fmt.Println(strDate)
	split := strings.Split(strDate, " ")
	i := 0
	switch split[0] {
	case "Mon":
		i = 1
	case "Tue":
		i = 2
	case "Web":
		i = 3
	case "Thu":
		i = 4
	case "Fri":
		i = 5
	case "Sat":
		i = 6
	case "Sun":
		i = 7
	}
	fmt.Println("Số ngày còn lại trong tuần: ", 8-i)
}

func Task7() {

	k := "TimeSave"
	timeSave := time.Now().UnixNano()
	fmt.Println(timeSave)
	ctx := context.WithValue(context.Background(), k, timeSave)
	time.Sleep(10 * time.Second)
	minusTime(ctx)
}

func minusTime(ctx context.Context) {
	if v := ctx.Value("TimeSave"); v != nil {
		result := time.Now().UnixNano() - v.(int64)
		fmt.Println("Hiệu giữa 2 khoảng thời gian:", result)
		return
	}
	fmt.Println("key not found:", "TimeSave")

}
func Task8() {

	ctx := context.Background()

	worker := NewScheduler()
	worker.Add(ctx, parseSubscriptionData, time.Second*5)
	// worker.Add(ctx, sendStatistics, time.Second*10)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)
	<-quit
	worker.Stop()

}

func parseSubscriptionData(ctx context.Context) {
	time.Sleep(time.Second * 1)
	fmt.Println(time.Now().Unix())
}
func sendStatistics(ctx context.Context) {
	time.Sleep(time.Second * 5)
	fmt.Printf("sub at %s\n", time.Now().String())
}
func Task9() {
	DurationOfTime := time.Duration(3) * time.Second

	// Defining function parameter of
	// AfterFunc() method
	f := func() {

		fmt.Println("i'm study")
	}

	Timer1 := time.AfterFunc(DurationOfTime, f)

	// Calling stop method
	// w.r.to Timer1
	defer Timer1.Stop()

	// Calling sleep method
	time.Sleep(10 * time.Second)
}
