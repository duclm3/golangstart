package main

import (
	"encoding/json"
	"fmt"
	"hello/Bai1"
	"hello/Bai2"
	"hello/Bai3"
	"hello/Bai4"
	"io/ioutil"
	"strings"
)

func Task1() {
	//phần 1: Đọc xuất mảng slice
	serve := Bai1.GetServe()
	fmt.Println("MẢng ban đầu:")
	fmt.Println(serve)
	fmt.Println()
	for k := range serve {
		fmt.Println(serve[k].Name)
		if strings.Contains(serve[k].Class, "Admin") {
			fmt.Println("Search admin: ", serve[k])
		}
	}
	fmt.Println()
	item := Bai1.Serve{Name: "fileCustome", Class: "org.cofax.cds.FileServlet.Custome"}
	serve = append(serve, item)
	for k := range serve {

		fmt.Printf("address of slice %p \n", &serve[k])
	}
	file, _ := json.MarshalIndent(serve, "", " ")

	_ = ioutil.WriteFile("./Bai1/test.json", file, 0644)
	//Phần 2: sort

	Bai1.SortArray()

	//Phần 3: Chuyển từ slice sang map
	Bai1.MUser()
}

func Task2() {
	//Phần 1:
	// fmt.Println("Phần 1:")
	// Bai2.CountTime()
	//Phần 2:
	// fmt.Println("Phần 2:")
	// var unixTime int64 = time.Now().Unix()
	// fmt.Println("Số ngày hiện tại: ",Bai2.Task2(unixTime))
	// Phần 3:
	// fmt.Println("Phần 3:")
	// Bai2.Task3()
	//Phần 4
	// fmt.Println("Phần 4:")
	// Bai2.CalculatorMinute()
	//Phần 5
	// fmt.Println("Phần 5: ")
	// Bai2.Task5()
	// //Phần 6
	// fmt.Println("Phần 6:Second")
	// Phần 7
	// Bai2.Task7()
	//Phần 8:
	Bai2.Task8()
	// Phần 9:
	// Bai2.Task9()

}
func Task3() {
	// Phần 1:
	// Bai3.Task1()
	//Phần 2:
	// Bai3.Task2()
	// //Phần 3:
	Bai3.Task3()
	//Phần 4:
	// Bai3.Task4()
}
func Task4() {
	Bai4.Migration()

	// Bai4.Insert_User(&user)
}
func main() {
	key := 3

	switch key {
	case 1:
		Task1()
		break
	case 2:
		Task2()
		break
	case 3:
		Task3()
		break
	case 4:
		Task4()
		break
	}
}
